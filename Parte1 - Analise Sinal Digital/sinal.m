clear;
close all;


fs = 100000;      %frequencia de amostragem
t = [0:1/fs:10];  %tempo
f = 20;           %frequencia do sinal

fr = 60;          %frequencia do ruido
A = 5;            %Amplitude do sinal
A_RUIDO = 1;      %Amplitude do ruido
perda_eco = 95;   %Porcentagem de perda de energia do eco
N = 40;             %Número de "harmonicos"
s = 0;
e = 0;
sinal_digital = 0;
eco = 0;

A_ECO = (100-perda_eco)*A/100;

%Sinal digital
for i=1:2:N %varie a quantidade de funcoes
  s = (A/i)*sin(2*pi*i*f*t);
  sinal_digital = sinal_digital + s;
end

%eco = sinal com defasagem de fase pela reflexão
for i=1:2:N
  e = (A_ECO/i)*sin(2*pi*i*f*t+pi/2);
  eco = eco + e;
end  

ruido_branco = awgn(sinal_digital, A_RUIDO, 'measured');

%ruído senoidal
ruido_senoidal = (A_RUIDO)*sin(2*pi*fr*t);

bus_c_eco = sinal_digital + eco;
bus_c_rb  = sinal_digital + ruido_branco;
bus_c_rs  = sinal_digital + ruido_senoidal;

SNR_DB = 20*log(A/A_RUIDO);

%grafico do sinal
plot(t,sinal_digital);
title('Sinal Digital no Barramento');
figure;

%grafico do sinal branco
plot(t,ruido_branco);
title('Ruído Branco');
figure;

%grafico do Eco
plot(t,eco);
title('Eco');
figure;

%grafico do sinal com eco
plot(t,bus_c_eco);
title('Sinal + Ruído');

my_fft(sinal_digital, fs);
my_fft(bus_c_rb, fs);
my_fft(bus_c_eco, fs);
my_fft(bus_c_rs, fs);