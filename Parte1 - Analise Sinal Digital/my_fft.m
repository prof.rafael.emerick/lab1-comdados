## Copyright (C) 2018 rezo
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} my_ftt (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: rezo <rezo@rezobook>
## Created: 2018-03-16

function [S, frequency] = my_fft (s, fs)

normal = length(s);
aux = 0:normal-1;
T = normal/fs;
frequency = aux/T;
S = fftn(s)/normal;
fc = ceil(normal/2);
S = S(1:fc);

figure();
plot(frequency(1:fc),abs(S));
title('Analise de Espectro');
xlabel('Frequencia (Hz)');
ylabel('Amplitude');

endfunction
